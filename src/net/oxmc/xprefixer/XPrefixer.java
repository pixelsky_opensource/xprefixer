package net.oxmc.xprefixer;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRegisterChannelEvent;
import org.bukkit.plugin.java.JavaPlugin;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class XPrefixer extends JavaPlugin
{
    static XPrefixer plugin;
    DisplayNameManager displayNameManager;

    public XPrefixer() {
        plugin = this;
    }

    public void onEnable()
    {
        this.displayNameManager = new DisplayNameManager(this);
        displayNameManager.init();

        getCommand("prefix").setExecutor(new XPrefCommandHandler());

        if (getServer().getPluginManager().getPlugin("PermissionsEx") != null)
        {
            getLogger().info("xPrefix system: handle PermissionsEx");
            getServer().getPluginManager().registerEvents(new Listener() {
                @EventHandler
                public void onJoin(PlayerJoinEvent event) {
                    Player player = event.getPlayer();
                    PermissionUser user = PermissionsEx.getPermissionManager().getUser(player);
                    String ownUserPrefix = user.getOwnPrefix();
                    String prefix = ownUserPrefix != null ? ChatColor.translateAlternateColorCodes('&', ownUserPrefix) : null;
                    XPrefixer.this.displayNameManager.setPrefix(player, prefix);
                }
            }, this);
        }

        getServer().getPluginManager().registerEvents(new Listener()
        {
            @EventHandler
            public void onJoin(PlayerRegisterChannelEvent event) {
                Player player = event.getPlayer();
                displayNameManager.sendAllDisplayNames(player);
                displayNameManager.sendUpdatedName(player.getName());
            }
        }, plugin);
    }
}